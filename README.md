# bill

#### 介绍

个人记账web应用后端

<a href="https://gitee.com/cyh724/bill-frontend" target="_blank">前端仓库地址</a>

#### 技术选型

- springboot:2.4.2

- mybatis-plus:3.4.2

- mysql:5.7


#### 项目预览


  项目地址：<a href="https://www.lazy-bill.cn/login?u=test&p=123456" target="_blank">懒人记账</a>
    
    账号：test
    
    密码：123456
    (请勿修改密码)

#### 启动方式

1. 将文件 *src/main/resources/db/bill.sql* 导入到mysql
2. 修改配置文件 *src/main/resources/application.yml* 数据库连接参数、启动端口等参数
3. 执行启动类 *src/main/java/org/rainwalk/bill/BillApplication.java*
4. 项目启动成功

#### 前后端单机部署方案（基于Docker）

之所以是单机部署方案，是因为我只有一台机器（逃.....

##### 后端服务

1. ###### 确保本机已装docker

   ```shell
   > docker version
   ```

2. ###### 建立目录 *bill-backend*（随意命名）

   ```shell
   > cd /yourpath
   > mkdir bill-backend
   ```

3. ###### *bill-backend* 目录中分别建立目录 *config*、*logs*、*history-log*，分别负责存放配置文件，运行时日志和历史日志

   ```shell
   > cd bill-backend
   > mkdir config
   > mkdir logs
   > mkdir history-log
   ```

4. ###### 进入 *config* 目录，拷贝项目配置文件

   ```shell
   > cd config
   > ls
   application-prod.yml  logback-spring.xml
   > cd ..
   ```

5. ###### 将项目打包，放到 *bill-backend* 目录下

   ```shell
   > ls
   bill-backend-0.0.1-SNAPSHOT.jar  config  history-log  logs
   ```

6. ###### 在目录 *bill-backend* 下创建文件 *Dockerfile*，编写以下内容

   ```shell
   > vim Dockerfile
   # Dockerfile文件内容
   
   FROM java:8
   MAINTAINER jack <jack724@126.com>
   ADD bill-backend-0.0.1-SNAPSHOT.jar /yourpath/bill-backend/bill.jar
   EXPOSE 8888
   RUN /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone
   ENTRYPOINT java -jar /yourpath/bill-backend/bill.jar --spring.config.location=/yourpath/bill-backend/config/application-prod.yml --logging.config=/yourpath/bill-backend/config/logback-spring.xml --spring.profiles.active=prod
   
   ```

7. ###### 在目录 *bill-backend* 下创建文件 *start.sh*，编写以下内容

   ```shell
   > vim start.sh
   # start.sh文件内容
   
   #路径
   SOURCE_PATH=.
   #docker 镜像/容器名字或者jar名字 这里都命名为这个
   SERVER_NAME=bill-backend
   TAG=1.0
   SERVER_PORT=8888
   #日志挂载
   LOG_PATH_HOST=/yourpath/bill-backend/logs
   LOG_PATH_DOCKER=/yourpath/bill-backend/logs
   #配置挂载
   CONFIG_PATH_HOST=/yourpath/bill-backend/config
   CONFIG_PATH_DOCKER=/yourpath/bill-backend/config
   #当前时间
   CURRENT_TIME=$(date "+%Y%m%d%H%M%S")
   ######################执行########################################
   echo "当前执行文件......$0"
   #拷贝日志
   echo "拷贝日志文件 logs -> histoty-log/logs-$CURRENT_TIME"
   cp -r logs history-log/logs-$(date "+%Y%m%d%H%M%S")
   #容器id
   CID=$(docker ps | grep "$SERVER_NAME" | awk '{print $1}')
   #镜像id
   IID=$(docker images | grep "$SERVER_NAME:$TAG" | awk '{print $3}')
   if [ -n "$CID" ]; then
     echo "存在容器$SERVER_NAME, CID-$CID"
     docker stop $SERVER_NAME
     docker rm $SERVER_NAME
   fi
   # 构建docker镜像
   if [ -n "$IID" ]; then
     echo "存在$SERVER_NAME:$TAG镜像，IID=$IID"
     docker rmi $SERVER_NAME:$TAG
   else
     echo "不存在$SERVER_NAME:$TAG镜像，开始构建镜像"
     cd $SOURCE_PATH
     docker build -t $SERVER_NAME:$TAG .
   fi
   # 运行docker容器
   docker run --name $SERVER_NAME -d -p $SERVER_PORT:$SERVER_PORT -v $LOG_PATH_HOST:$LOG_PATH_DOCKER -v $CONFIG_PATH_HOST:$CONFIG_PATH_DOCKER $SERVER_NAME:$TAG
   echo "$SERVER_NAME容器创建完成"
   ```

8. ###### 执行文件 *start.sh*

   ```shell
   > ./start.sh
   
   当前执行文件......./start.sh
   拷贝日志文件 logs -> histoty-log/logs-20210316164836
   不存在bill-backend:1.0镜像，开始构建镜像
   Sending build context to Docker daemon 59.72 MB
   Step 1/5 : FROM java:8
    ---> d23bdf5b1b1b
   Step 2/5 : MAINTAINER jack <jack724@126.com>
    ---> Using cache
    ---> 2a2390a72396
   Step 3/5 : ADD bill-backend-0.0.1-SNAPSHOT.jar usr/local/bill/bill.jar
    ---> Using cache
    ---> 9e1442b65c40
   Step 4/5 : EXPOSE 8888
    ---> Using cache
    ---> 98fc48253165
   Step 5/5 : ENTRYPOINT java -jar /yourpath/bill-backend/bill.jar --spring.config.location=/yourpath/bill-backend/config/application-prod.yml --logging.config=/yourpath/bill-backend/config/logback-spring.xml --spring.profiles.active=prod
    ---> Running in 32f0f314ddfd
    ---> bac8a25ad3d1
   Removing intermediate container 32f0f314ddfd
   Successfully built baw8a25ad3d1
   b1deff858afec44b18159f82be7edc568c3a37d785b2216ce7d5258d2da52205
   bill-backend容器创建完成
   ```

9. ###### 项目启动完毕，可以使用 *docker ps* 命令查看是否启动成功

   ```shell
   > docker ps
   
   CONTAINER ID    IMAGE               COMMAND                  CREATED          STATUS          PORTS                     NAMES
   b1deff858afe    bill-backend:1.0  "/bin/sh -c 'java ..."   22 minutes ago   Up 22 minutes   0.0.0.0:8888->8888/tcp    bill-backend
   
   ```

10. ###### 如果启动失败可以使用 docker logs 查看启动日志

    ```shell
    > docker logs bill-backend
    ```



##### 前端服务

1. ###### 确保本机已装docker

   ```shell
   > docker version
   ```

2. ###### 建立目录 *bill-frontend*（随意命名）

   ```shell
   > cd /yourpath
   > mkdir bill-frontend
   ```

3. ###### *bill-frontend* 目录中建立目录 *nginx_log*，存放nginx日志

   ```shell
   > cd bill-frontend
   > mkdir nginx_log
   ```

4. ###### 创建nginx配置文件 *default.conf*

   ```shell
   > docker inspect --format '{{.NetworkSettings.IPAddress}}' bill-java
   dockerip
   > vim default.conf
   
   # default.conf文件内容
   server {
       listen       80;
       server_name  localhost;

       gzip  on;   #开启gzip
       gzip_min_length 1k; #低于1kb的资源不压缩
       gzip_comp_level 3; #压缩级别【1-9】，越大压缩率越高，同时消耗cpu资源也越多，建议设置在4左右。
       gzip_types text/plain application/javascript application/x-javascript text/javascript text/xml text/css;  #需要压缩哪些响应类型的资源
       gzip_disable "MSIE [1-6]\.";  #配置禁用gzip条件，支持正则。此处表示ie6及以下不启用gzip（因为ie低版本不支持）
   
       #charset koi8-r;
       access_log  /var/log/nginx/host.access.log  main;
       error_log  /var/log/nginx/error.log  error;
   
       location / {
           root   /usr/share/nginx/html;
           index  index.html index.htm;
           try_files $uri $uri/ /index.html;
       }
   
       location /bill/v1 {
           proxy_set_header Host $http_host;
           proxy_set_header X-Real-IP $remote_addr;
           proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
           proxy_set_header X-Forwarded-Proto $scheme;
           proxy_pass http://backend-ip:backend-port/bill;
       }
   
       #error_page  404              /404.html;
   
       # redirect server error pages to the static page /50x.html
       #
       error_page   500 502 503 504  /50x.html;
       location = /50x.html {
           root   /usr/share/nginx/html;
       }
   }
   ```

5. ###### 将项目打包，放到 *bill-frontend* 目录下

   ```shell
   > ls
   default.conf  dist  nginx_log
   ```

6. ###### 在目录 *bill-frontend* 下创建文件 *Dockerfile*，编写以下内容

   ```shell
   > vim Dockerfile
   # Dockerfile文件内容
   
   FROM nginx
   MAINTAINER jack <jack724@126.com> #Author
   RUN rm /etc/nginx/conf.d/default.conf
   ADD default.conf /etc/nginx/conf.d/
   COPY ./dist/ /usr/share/nginx/html/
   RUN echo '镜像构建成功!' #输出完成
   ```

7. ###### 在目录 *bill-frontend* 下创建文件 *start.sh*，编写以下内容

   ```shell
   > vim start.sh
   # start.sh文件内容
   
   #dockerfile路径
   SOURCE_PATH=.
   #docker 镜像/容器名字
   SERVER_NAME=bill-vue
   TAG=1.0
   SERVER_PORT=80
   DOCKER_PORT=80
   NGINX_LOG_HOST=/usr/local/bill/vue/nginx_log
   NGINX_LOG_DOCKER=/var/log/nginx
   ######################执行########################################
   echo "当前执行文件......$0"
   #容器id
   CID=$(docker ps | grep "$SERVER_NAME" | awk '{print $1}')
   #镜像id
   IID=$(docker images | grep "$SERVER_NAME:$TAG" | awk '{print $3}')
   if [ -n "$CID" ]; then
     echo "存在容器$SERVER_NAME, CID-$CID"
     docker stop $SERVER_NAME
     docker rm $SERVER_NAME
   fi
   # 构建docker镜像
   if [ -n "$IID" ]; then
     echo "存在$SERVER_NAME:$TAG镜像，IID=$IID"
     docker rmi $SERVER_NAME:$TAG
   fi
   echo "不存在$SERVER_NAME:$TAG镜像，开始构建镜像"
   cd $SOURCE_PATH
   docker build -t $SERVER_NAME:$TAG .
   # 运行docker容器
   docker run --name $SERVER_NAME -d -p $SERVER_PORT:$DOCKER_PORT -v $NGINX_LOG_HOST:$NGINX_LOG_DOCKER  $SERVER_NAME:$TAG
   echo "$SERVER_NAME容器创建完成"
   ```

8. ###### 执行文件 *start.sh*

   ```shell
   > ./start.sh
   
   当前执行文件......./start.sh
   不存在bill-vue:1.0镜像，开始构建镜像
   Sending build context to Docker daemon 13.17 MB
   Step 1/6 : FROM nginx
    ---> f6d0b4767a6c
   Step 2/6 : MAINTAINER jack <jack724@126.com> #Author
    ---> Using cache
    ---> f48e5d356a43
   Step 3/6 : RUN rm /etc/nginx/conf.d/default.conf
    ---> Using cache
    ---> 47366958c4e6
   Step 4/6 : ADD default.conf /etc/nginx/conf.d/
    ---> 3b50797897ed
   Removing intermediate container a2880d1563b9
   Step 5/6 : COPY ./dist/ /usr/share/nginx/html/
    ---> 50a9fbd589fb
   Removing intermediate container fa4842c62501
   Step 6/6 : RUN echo '镜像构建成功!' #输出完成
    ---> Running in 3df3bd688770
   
   镜像构建成功!
    ---> e9b049f9c56d
   Removing intermediate container 3df3bd688770
   Successfully built e9b049f9c56d
   9d1b15ffa005e044744733bbe7ad2b4c288eefdc04861d4014af380fd5cb414f
   bill-vue容器创建完成
   ```

9. ###### 项目启动完毕，可以使用 *docker ps* 命令查看是否启动成功

   ```shell
   > docker ps
   
   CONTAINER ID    IMAGE          COMMAND                  CREATED          STATUS          PORTS                     NAMES
   9d1b15ffa005    bill-vue:1.0   "/docker-entrypoin..."   2 minutes ago    Up 2 minutes    0.0.0.0:80->80/tcp        bill-vue
   
   
   ```

10. ###### 如果启动失败可以使用 docker logs 查看启动日志

    ```shell
    > docker logs bill-frontend
    ```