package org.rainwalk.bill.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 类别图标-iconfont图标库 前端控制器
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
@RestController
@RequestMapping("/icon")
public class IconController {

}

