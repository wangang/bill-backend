package org.rainwalk.bill.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.rainwalk.bill.api.formatter.annotation.EnumFormatter;
import org.rainwalk.bill.api.formatter.annotation.MoneyFormatter;
import org.rainwalk.bill.api.interceptor.AuthInterceptor;
import org.rainwalk.bill.api.model.Response;
import org.rainwalk.bill.api.model.SessionUser;
import org.rainwalk.bill.model.dto.DateDTO;
import org.rainwalk.bill.model.enums.BillChargeTypeEnum;
import org.rainwalk.bill.model.enums.BillTypeEnum;
import org.rainwalk.bill.model.vo.BillTimeVO;
import org.rainwalk.bill.model.vo.CategoryProportionVO;
import org.rainwalk.bill.model.vo.ExpenditureBarVO;
import org.rainwalk.bill.service.IChartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.Collections;
import java.util.List;

/**
 * 图表接口
 *
 * @author chenyuheng create class on 2021-02-24
 */
@Api(tags = "图表模块")
@RestController
@RequestMapping("/charts")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ChartsController {

    private final IChartsService chartsService;

    @ApiOperation("获取账单时间范围")
    @GetMapping("/time")
    public Response getBillTime(@SessionAttribute(AuthInterceptor.USER_SESSION) SessionUser sessionUser) {
        BillTimeVO billTime = this.chartsService.getBillTime(sessionUser.getId());
        return Response.success(billTime);
    }

    @ApiOperation("获取支出分类占比数据")
    @GetMapping("/expenditure/categoryProportion")
    public Response expenditureCategoryProportion(@Validated DateDTO dto, @SessionAttribute(AuthInterceptor.USER_SESSION) SessionUser sessionUser) {
        if (dto.getStart() > dto.getEnd()) {
            return Response.success(Collections.emptyList());
        }
        List<CategoryProportionVO> vos = chartsService.categoryProportion(dto.getStart(), dto.getEnd(), sessionUser.getId(), BillChargeTypeEnum.EXPENDITURE);
        return Response.success(vos);
    }

    @ApiOperation("获取收入分类占比数据")
    @GetMapping("/income/categoryProportion")
    public Response incomeCategoryProportion(@Validated DateDTO dto, @SessionAttribute(AuthInterceptor.USER_SESSION) SessionUser sessionUser) {
        if (dto.getStart() > dto.getEnd()) {
            return Response.success(Collections.emptyList());
        }
        List<CategoryProportionVO> vos = chartsService.categoryProportion(dto.getStart(), dto.getEnd(), sessionUser.getId(), BillChargeTypeEnum.INCOME);
        return Response.success(vos);
    }

    @ApiOperation("获取支出分类柱状数据")
    @GetMapping("/expenditure/bar")
    @EnumFormatter(field = "billType", enumClazz = BillTypeEnum.class)
    @MoneyFormatter(fields = "amount")
    public Response expenditureBar(@Validated DateDTO dto, @SessionAttribute(AuthInterceptor.USER_SESSION) SessionUser sessionUser) {
        if (dto.getStart() > dto.getEnd()) {
            return Response.success(Collections.emptyList());
        }
        List<ExpenditureBarVO> bar = chartsService.bar(dto.getStart(), dto.getEnd(), sessionUser.getId(), BillChargeTypeEnum.EXPENDITURE);
        return Response.success(bar);
    }

}
