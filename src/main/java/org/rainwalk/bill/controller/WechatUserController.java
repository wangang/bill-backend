package org.rainwalk.bill.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 微信小程序用户表 前端控制器
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-16
 */
@RestController
@RequestMapping("/wechat-user")
public class WechatUserController {

}

