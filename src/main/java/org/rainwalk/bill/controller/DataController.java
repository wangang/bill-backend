package org.rainwalk.bill.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.rainwalk.bill.api.interceptor.AuthInterceptor;
import org.rainwalk.bill.api.model.Response;
import org.rainwalk.bill.api.model.SessionUser;
import org.rainwalk.bill.entity.Category;
import org.rainwalk.bill.model.enums.*;
import org.rainwalk.bill.model.vo.DataVO;
import org.rainwalk.bill.model.vo.UserCategoryVO;
import org.rainwalk.bill.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * 数据接口
 *
 * @author 趁雨行
 */
@Api(tags = "数据模块")
@RestController
@RequestMapping("/data")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DataController {

    public static final Collator CHINESE_TEXT = Collator.getInstance(Locale.CHINA);
    private final ICategoryService categoryService;

    @ApiOperation("计费类型数据")
    @GetMapping("/chargeType")
    public Response chargeType() {

        return Response.success(getByDescValueEnum(BillChargeTypeEnum.class));
    }

    @ApiOperation("交易状态数据")
    @GetMapping("/state")
    public Response state() {
        return Response.success(getByDescValueEnum(BillStateEnum.class));
    }

    @ApiOperation("类别数据")
    @GetMapping("/category")
    public Response category(@SessionAttribute(AuthInterceptor.USER_SESSION) SessionUser sessionUser) {
        List<UserCategoryVO> userCategoryVOS = categoryService.queryByUser(sessionUser.getId());
        List<DataVO> categories = userCategoryVOS.stream()
                .sorted((x, y) -> CHINESE_TEXT.compare(x.getCategoryName(), y.getCategoryName()))
                .map(category -> new DataVO(category.getId(), category))
                .collect(Collectors.toList());
        UserCategoryVO unclassified = new UserCategoryVO();
        unclassified.setId("unclassified");
        unclassified.setCategoryName("未分类");
        categories.add(0, new DataVO(unclassified.getId(), unclassified));
        return Response.success(categories);
    }

    @ApiOperation("账单类型字典")
    @GetMapping("/billType")
    public Response billType() {
        return Response.success(getByDescValueEnum(BillTypeEnum.class));
    }

    @ApiOperation("排序类型字典")
    @GetMapping("/sort")
    public Response sort() {
        return Response.success(getByDescValueEnum(BillQuerySortEnum.class));
    }

    private List<DataVO> getByDescValueEnum(Class enumClazz) {
        if (!DescValueEnum.class.isAssignableFrom(enumClazz)) {
            return new ArrayList<>(0);
        }
        DescValueEnum[] constants = (DescValueEnum[]) enumClazz.getEnumConstants();
        List<DataVO> vos = new ArrayList<>(constants.length);
        for (DescValueEnum constant : constants) {
            DataVO dataVO = new DataVO(constant.getValue(), constant.getDesc());
            vos.add(dataVO);
        }
        return vos;
    }
}
