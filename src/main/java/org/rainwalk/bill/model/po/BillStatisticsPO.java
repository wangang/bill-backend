package org.rainwalk.bill.model.po;

import org.rainwalk.bill.model.vo.BillStatisticsVO;

public class BillStatisticsPO {

    private Integer differ;

    private Integer sum;

    private Integer serviceFee;

    private Integer refundAmount;


    public BillStatisticsVO toVO() {
        int expenditure = (sum - differ)/2;
        int income = sum - expenditure;
        return new BillStatisticsVO(expenditure, income, serviceFee, refundAmount);
    }
}
