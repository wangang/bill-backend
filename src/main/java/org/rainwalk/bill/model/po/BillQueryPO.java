package org.rainwalk.bill.model.po;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BillQueryPO {

    /**
     * 交易单号
     */
    private final String transactionNo;

    /**
     * 商户单号
     */
    private final String merchantOrderNo;

    /**
     * 交易时间（开始）
     */
    private final Integer createTimeBegin;

    /**
     * 交易时间（结束）
     */
    private final Integer createTimeEnd;

    /**
     * 交易时间点（开始）
     */
    private final Integer createTimePointBegin;

    /**
     * 交易时间点（结束）
     */
    private final Integer createTimePointEnd;

    /**
     * 交易类型
     */
    private final String transactionType;

    /**
     * 交易对方
     */
    private final String otherParty;

    /**
     * 商品名称
     */
    private final String goodsName;

    /**
     * 交易金额(元)：最低
     */
    private final Integer amountMin;

    /**
     * 交易金额(元)：最高
     */
    private final Integer amountMax;

    /**
     * 计费类型 -1：支出  0：未知  1：收入
     */
    private final Integer[] chargeType;

    /**
     * 交易状态描述
     */
    private final String stateDesc;

    /**
     * 交易状态 1：成功 2：其他
     */
    private final Integer[] state;

    /**
     * 服务费：最低
     */
    private final Integer serviceFeeMin;

    /**
     * 服务费：最高
     */
    private final Integer serviceFeeMax;

    /**
     * 成功退款：最低
     */
    private final Integer refundAmountMin;

    /**
     * 成功退款：最高
     */
    private final Integer refundAmountMax;

    /**
     * 备注
     */
    private final String remark;

    /**
     * 账单类型 0：用户写入 1：微信 2：支付宝
     */
    private final Integer[] billType;

    /**
     * 用户备注
     */
    private final String userRemark;

    /**
     * 用户消费类别id
     */
    private final String[] categoryId;

    /**
     * 落库批次号
     */
    private final String batchNo;

    /**
     * 页码
     */
    private final Integer pageNum;

    /**
     * 每页大小
     */
    private final Integer pageSize;

    /**
     * sql查询起始行
     */
    private final Integer startRow;

    /**
     * 用户id
     */
    private final String appUserId;

    /**
     * 排序规则
     */
    private final String sort;
}
