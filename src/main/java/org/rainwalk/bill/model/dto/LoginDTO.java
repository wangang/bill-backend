package org.rainwalk.bill.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登陆参数
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Data
@ApiModel("登录表单")
public class LoginDTO {

    @ApiModelProperty(value = "账号", required = true, example = "test")
    @NotBlank(message = "请输入账号")
    private String username;

    @ApiModelProperty(value = "密码", required = true, example = "e10adc3949ba59abbe56e057f20f883e")
    @NotBlank(message = "请输入密码")
    private String password;

}
