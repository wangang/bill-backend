package org.rainwalk.bill.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 日期表单
 *
 * @author chenyuheng create class on 2021-02-24
 */
@Data
@ApiModel("日期表单")
public class DateDTO {

    @ApiModelProperty(value = "开始时间（结束）", example = "1614873600")
    @NotNull(message = "开始时间不能为空")
    private Integer start;

    @ApiModelProperty(value = "截止时间（结束）", example = "1614873600")
    @NotNull(message = "截止时间不能为空")
    private Integer end;

}
