package org.rainwalk.bill.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 修改密码表单
 *
 * @author 趁雨行
 */
@Data
@ApiModel("修改密码表单")
public class PasswordUpdateDTO {

    /**
     * 旧密码
     */
    @ApiModelProperty(value = "旧密码", required = true, example = "123456")
    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;

    /**
     * 新密码
     */
    @ApiModelProperty(value = "新密码", required = true, example = "123456")
    @NotBlank(message = "新密码不能为空")
    private String newPassword;

}
