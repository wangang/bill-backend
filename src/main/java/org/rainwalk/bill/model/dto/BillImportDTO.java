package org.rainwalk.bill.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;


@Data
@ApiModel("导入账单文件表单")
public class BillImportDTO {

    @ApiModelProperty(value = "账单文件", required = true)
    @NotNull(message = "请上传文件")
    private MultipartFile file;

    @ApiModelProperty(value = "账单文件", required = true, example = "true")
    @NotNull(message = "请指定是否忽略重复订单")
    private Boolean ignoreDuplicate;

}
