package org.rainwalk.bill.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.rainwalk.bill.valid.PatternNumber;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@ApiModel("账单更新表单")
public class BillUpdateDTO {


    /**
     * 交易时间
     */
    @ApiModelProperty(value = "交易时间", example = "2021-10-15 12:00:00")
    @Pattern(regexp = "^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\\s+(20|21|22|23|[0-1]\\d):[0-5]\\d:[0-5]\\d$", message = "日期格式错误")
    private String createTime;

    /**
     * 交易类型
     */
    @ApiModelProperty(value = "交易类型", example = "即时到账交易")
    private String transactionType;

    /**
     * 交易对方
     */
    @ApiModelProperty(value = "交易对方", example = "饿了么")
    private String otherParty;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", example = "饿了么准时达")
    private String goodsName;

    /**
     * 交易金额
     */
    @ApiModelProperty(value = "交易金额", example = "0.22")
    @NotBlank(message = "请填写交易金额")
    private String amount;

    /**
     * 计费类型 -1：支出  0：未知  1：收入
     */
    @ApiModelProperty(value = "计费类型", example = "1")
    @PatternNumber(regexp = "^(-1|0|1)$", message = "计费类型有误")
    @NotNull(message = "请选择计费类型")
    private Integer chargeType;

    /**
     * 交易状态描述
     */
    @ApiModelProperty(value = "交易状态描述", example = "已支出")
    private String stateDesc;

    /**
     * 交易状态 1：成功 2：其他
     */
    @ApiModelProperty(value = "交易状态", example = "1")
    @PatternNumber(regexp = "^([12])$", message = "交易状态有误")
    @NotNull(message = "请选择交易状态")
    private Integer state;

    /**
     * 服务费
     */
    @ApiModelProperty(value = "服务费", example = "0.01")
    @NotBlank(message = "请填写服务费")
    private String serviceFee;

    /**
     * 成功退款
     */
    @ApiModelProperty(value = "成功退款金额", example = "0.01")
    @NotBlank(message = "请填写退款金额")
    private String refundAmount;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", example = "账单备注")
    private String remark;

    /**
     * 用户备注
     */
    @ApiModelProperty(value = "用户备注", example = "用户备注")
    private String userRemark;

    /**
     * 用户消费类别
     */
    @ApiModelProperty(value = "用户消费类别id", example = "1")
    private String categoryId;


}
