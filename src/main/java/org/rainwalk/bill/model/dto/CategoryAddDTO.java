package org.rainwalk.bill.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("增加类别表单")
public class CategoryAddDTO {

    @ApiModelProperty(value = "类别名称", required = true, example = "分类1")
    @NotBlank(message = "分类名称不能为空")
    private String categoryName;

}
