package org.rainwalk.bill.model.dto;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;
import org.rainwalk.bill.model.enums.BillQuerySortEnum;
import org.rainwalk.bill.model.po.BillQueryPO;
import org.rainwalk.bill.util.MoneyUtil;
import org.rainwalk.bill.util.TimeUtil;

import javax.validation.constraints.Pattern;

/**
 * 账单查询条件
 *
 * @author chenyuheng create class on 2021-02-20
 */
@Data
@ApiModel("账单查询表单")
public class BillQueryDTO {

    public BillQueryDTO() {
        this.pageNum = 1;
        this.pageSize = 10;
    }

    /**
     * 交易单号
     */
    @ApiModelProperty(value = "交易单号", example = "2534214875454")
    private String transactionNo;

    /**
     * 商户单号
     */
    @ApiModelProperty(value = "商户单号", example = "2534214875454")
    private String merchantOrderNo;

    /**
     * 交易时间（开始）
     */
    @ApiModelProperty(value = "交易时间（开始）", example = "2021-10-15 12:00:00")
    private String createTimeBegin;

    /**
     * 交易时间（结束）
     */
    @ApiModelProperty(value = "交易时间（结束）", example = "2021-10-15 12:00:00")
    private String createTimeEnd;

    /**
     * 交易时间点（开始）
     */
    @ApiModelProperty(value = "交易时间点（开始）", example = "12:00:00")
    private String createTimePointBegin;

    /**
     * 交易时间点（结束）
     */
    @ApiModelProperty(value = "交易时间点（结束）", example = "12:00:00")
    private String createTimePointEnd;

    /**
     * 交易类型
     */
    @ApiModelProperty(value = "交易类型", example = "即时到账交易")
    private String transactionType;

    /**
     * 交易对方
     */
    @ApiModelProperty(value = "交易对方", example = "饿了么")
    private String otherParty;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称", example = "饿了么准时达")
    private String goodsName;

    /**
     * 交易金额(元)：最低
     */
    @ApiModelProperty(value = "交易金额(元)：最低", example = "0.22")
    @Pattern(regexp = "(^$)|^([1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)", message = "最低交易金额错误")
    private String amountMin;

    /**
     * 交易金额(元)：最高
     */
    @ApiModelProperty(value = "交易金额(元)：最高", example = "100.22")
    @Pattern(regexp = "(^$)|^([1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)", message = "最高交易金额错误")
    private String amountMax;

    /**
     * 计费类型 -1：支出  0：未知  1：收入
     */
    @ApiModelProperty(value = "计费类型", example = "[0,1]")
    private Integer[] chargeType;

    /**
     * 交易状态描述
     */
    @ApiModelProperty(value = "交易状态描述", example = "已支出")
    private String stateDesc;

    /**
     * 交易状态 1：成功 2：其他
     */
    @ApiModelProperty(value = "计费类型", example = "[1,2]")
    private Integer[] state;

    /**
     * 服务费：最低
     */
    @ApiModelProperty(value = "服务费：最低", example = "0.22")
    @Pattern(regexp = "(^$)|^([1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)", message = "最低服务费错误")
    private String serviceFeeMin;

    /**
     * 服务费：最高
     */
    @ApiModelProperty(value = "服务费：最高", example = "100.22")
    @Pattern(regexp = "(^$)|^([1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)", message = "最高服务费错误")
    private String serviceFeeMax;

    /**
     * 成功退款：最低
     */
    @ApiModelProperty(value = "成功退款：最低", example = "0.22")
    @Pattern(regexp = "(^$)|^([1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)", message = "最低成功退款错误")
    private String refundAmountMin;

    /**
     * 成功退款：最高
     */
    @ApiModelProperty(value = "成功退款：最低", example = "50.22")
    @Pattern(regexp = "(^$)|^([1-9]([0-9]+)?(\\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\\.[0-9]([0-9])?$)", message = "最高成功退款错误")
    private String refundAmountMax;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注", example = "账单备注")
    private String remark;

    /**
     * 账单类型 0：用户写入 1：微信 2：支付宝
     */
    @ApiModelProperty(value = "账单类型", example = "[1,2]")
    private Integer[] billType;

    /**
     * 用户备注
     */
    @ApiModelProperty(value = "用户备注", example = "用户备注")
    private String userRemark;

    /**
     * 用户消费类别id
     */
    @ApiModelProperty(value = "用户消费类别id", example = "[1,2]")
    private String[] categoryId;

    /**
     * 落库批次号
     */
    @ApiModelProperty(value = "落库批次号", example = "1512510620")
    private String batchNo;

    /**
     * 页码
     */
    @ApiModelProperty(value = "页码", example = "1")
    private Integer pageNum;

    /**
     * 每页大小
     */
    @ApiModelProperty(value = "每页大小", example = "10")
    private Integer pageSize;

    /**
     * 排序规则
     */
    @ApiModelProperty(value = "排序规则", example = "create_time DESC")
    @Pattern(regexp = "(^$)|^(create_time DESC)|(create_time ASC)|(amount DESC)|(amount ASC)$", message = "无效排序")
    private String sort;


    public BillQueryPO toPo(@NonNull String appUserId) {
        return BillQueryPO.builder()
                .transactionNo(StrUtil.trimToNull(transactionNo))
                .merchantOrderNo(StrUtil.trimToNull(merchantOrderNo))
                .createTimeBegin(StrUtil.isNotBlank(createTimeBegin) ? TimeUtil.toTimestamp(createTimeBegin) : null)
                .createTimeEnd(StrUtil.isNotBlank(createTimeEnd) ? TimeUtil.toTimestamp(createTimeEnd) : null)
                .createTimePointBegin(StrUtil.isNotBlank(createTimePointBegin) ? TimeUtil.toTimePointFormat(createTimePointBegin) : null)
                .createTimePointEnd(StrUtil.isNotBlank(createTimePointEnd) ? TimeUtil.toTimePointFormat(createTimePointEnd) : null)
                .transactionType(StrUtil.trimToNull(transactionType))
                .otherParty(StrUtil.trimToNull(otherParty))
                .goodsName(StrUtil.trimToNull(goodsName))
                .amountMin(StrUtil.isNotBlank(amountMin) ? MoneyUtil.yuan2fen(amountMin) : null)
                .amountMax(StrUtil.isNotBlank(amountMax) ? MoneyUtil.yuan2fen(amountMax) : null)
                .chargeType(chargeType)
                .stateDesc(StrUtil.trimToNull(stateDesc))
                .state(state)
                .serviceFeeMin(StrUtil.isNotBlank(serviceFeeMin) ? MoneyUtil.yuan2fen(serviceFeeMin) : null)
                .serviceFeeMax(StrUtil.isNotBlank(serviceFeeMax) ? MoneyUtil.yuan2fen(serviceFeeMax) : null)
                .refundAmountMin(StrUtil.isNotBlank(refundAmountMin) ? MoneyUtil.yuan2fen(refundAmountMin) : null)
                .refundAmountMax(StrUtil.isNotBlank(refundAmountMax) ? MoneyUtil.yuan2fen(refundAmountMax) : null)
                .remark(StrUtil.trimToNull(remark))
                .billType(billType)
                .userRemark(StrUtil.trimToNull(userRemark))
                .categoryId(categoryId)
                .batchNo(StrUtil.trimToNull(batchNo))
                .pageNum(pageNum)
                .pageSize(pageSize)
                .startRow((pageNum - 1) * pageSize)
                .appUserId(appUserId)
                .sort(StrUtil.isBlank(sort) ? BillQuerySortEnum.create_time_desc.getValue() : sort)
                .build();
    }

}
