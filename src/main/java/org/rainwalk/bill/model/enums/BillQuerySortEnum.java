package org.rainwalk.bill.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  BillQuerySortEnum implements DescValueEnum {

    create_time_desc("时间 ↓", "create_time DESC"),
    create_time_asc("时间 ↑", "create_time ASC"),
    amount_desc("金额 ↓", "amount DESC"),
    amount_asc("金额 ↑", "amount ASC")
    ;

    private final String desc;

    private final String value;

}
