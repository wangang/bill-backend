package org.rainwalk.bill.model.enums;

/**
 * 描述-值 对应枚举接口
 *
 * @author chenyuheng create class on 2021-02-20
 */
public interface DescValueEnum {

    /**
     * 根据值获取描述
     *
     * @param value
     * @param clazz
     * @return
     */
    static DescValueEnum getByValue(Object value, Class<? extends DescValueEnum> clazz){
        for (DescValueEnum i : clazz.getEnumConstants()) {
            if (i.getValue().equals(value)) {
                return i;
            }
        }
        return null;
    }

    /**
     * 获取值
     *
     * @return
     */
    Object getValue();

    /**
     * 获取描述
     *
     * @return
     */
    Object getDesc();

}
