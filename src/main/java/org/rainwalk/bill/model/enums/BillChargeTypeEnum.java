package org.rainwalk.bill.model.enums;

import lombok.Getter;

@Getter
public enum  BillChargeTypeEnum implements DescValueEnum {

    EXPENDITURE("支出", -1),
    INCOME("收入", 1),
    UNKNOWN("中性", 0);

    private String desc;

    private Integer value;

    BillChargeTypeEnum(String desc, Integer value) {
        this.desc = desc;
        this.value = value;
    }

    public static BillChargeTypeEnum getByAmount(String chargeType) {
        if (chargeType.contains("支")) {
            return EXPENDITURE;
        }

        if (chargeType.contains("收")) {
            return INCOME;
        }

        return UNKNOWN;
    }
}
