package org.rainwalk.bill.model.enums;

import lombok.Getter;

/**
 * 账单交易状态
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Getter
public enum BillStateEnum implements DescValueEnum {

    SUCCESS("成功", 1),
    OTHER("其他", 2);

    private String desc;

    private Integer value;

    BillStateEnum(String desc, Integer value) {
        this.desc = desc;
        this.value = value;
    }

}
