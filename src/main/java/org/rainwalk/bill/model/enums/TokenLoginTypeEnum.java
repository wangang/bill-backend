package org.rainwalk.bill.model.enums;

import lombok.Getter;

/**
 * 登录类型
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Getter
public enum TokenLoginTypeEnum implements DescValueEnum {

    PC("PC", 0),
    WECHAT_MINI_APP("微信小程序", 1);

    private String desc;

    private Integer value;

    TokenLoginTypeEnum(String desc, Integer value) {
        this.desc = desc;
        this.value = value;
    }

}
