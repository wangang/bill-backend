package org.rainwalk.bill.model.enums;

import lombok.Getter;

@Getter
public enum AppUserAuthorizedEnum implements DescValueEnum {

    UNAUTHORIZED("未授权", 0),
    AUTHORIZED("已授权",1),
    ;

    private String desc;

    private Integer value;


    AppUserAuthorizedEnum(String desc, Integer value) {
        this.value = value;
        this.desc = desc;
    }
}
