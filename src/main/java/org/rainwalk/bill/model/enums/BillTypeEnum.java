package org.rainwalk.bill.model.enums;

import lombok.Getter;

/**
 * 账单交易状态
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Getter
public enum BillTypeEnum implements DescValueEnum {

    CUSTOMER("手动添加", 0),
    WECHAT("微信", 1),
    ALIPAY("支付宝", 2);

    private String desc;

    private Integer value;

    BillTypeEnum(String desc, Integer value) {
        this.desc = desc;
        this.value = value;
    }

}
