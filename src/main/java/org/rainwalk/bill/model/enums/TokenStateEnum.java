package org.rainwalk.bill.model.enums;

import lombok.Getter;

/**
 * token状态
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Getter
public enum TokenStateEnum implements DescValueEnum {

    EFFECTIVE("有效", 0),
    INVALID("无效", 1);

    private String desc;

    private Integer value;

    TokenStateEnum(String desc, Integer value) {
        this.desc = desc;
        this.value = value;
    }

}
