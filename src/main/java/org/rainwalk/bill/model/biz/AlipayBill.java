package org.rainwalk.bill.model.biz;

import cn.hutool.core.annotation.Alias;
import lombok.Data;
import org.rainwalk.bill.entity.Bill;
import org.rainwalk.bill.model.enums.BillChargeTypeEnum;
import org.rainwalk.bill.model.enums.BillStateEnum;
import org.rainwalk.bill.model.enums.BillTypeEnum;
import org.rainwalk.bill.util.IdGenerator;
import org.rainwalk.bill.util.MoneyUtil;
import org.rainwalk.bill.util.TimeUtil;

/**
 * 支付宝账单结构
 *
 * @author chenyuheng create class on 2021-02-19
 */
@Data
public class AlipayBill {


    @Alias("交易号")
    private String transactionNo;

    @Alias("商家订单号")
    private String merchantOrderNo;

    @Alias("交易创建时间")
    private String createTime;

    @Alias("付款时间")
    private String payTime;

    @Alias("最近修改时间")
    private String updateTime;

    @Alias("交易来源地")
    private String origin;

    @Alias("类型")
    private String transactionType;

    @Alias("交易对方")
    private String otherParty;

    @Alias("商品名称")
    private String goodsName;

    @Alias("金额（元）")
    private String amount;

    @Alias("收/支")
    private String chargeType;

    @Alias("交易状态")
    private String state;

    @Alias("服务费（元）")
    private String serviceFee;

    @Alias("成功退款（元）")
    private String refundAmount;

    @Alias("备注")
    private String remark;

    @Alias("资金状态")
    private String stateDesc;

    public Bill toBill(String appUserId, String batchNo) {
        Bill bill = new Bill();
        bill.setId(IdGenerator.generateId());
        bill.setTransactionNo(this.transactionNo.trim());
        bill.setMerchantOrderNo(this.merchantOrderNo.trim());
        bill.setCreateTime(TimeUtil.toTimestamp(this.createTime.trim()));
        bill.setCreateTimePoint(TimeUtil.toTimePoint(this.createTime.trim()));
        bill.setTransactionType(this.transactionType.trim());
        bill.setOtherParty(this.otherParty.trim());
        bill.setGoodsName(this.goodsName.trim());
        bill.setAmount(MoneyUtil.yuan2fen(this.amount.trim()));
        bill.setChargeType(BillChargeTypeEnum.getByAmount(this.chargeType.trim()).getValue());
        bill.setStateDesc(this.stateDesc.trim());
        bill.setState(BillStateEnum.SUCCESS.getValue());
        bill.setServiceFee(MoneyUtil.yuan2fen(this.serviceFee.trim()));
        bill.setRefundAmount(MoneyUtil.yuan2fen(this.refundAmount.trim()));
        bill.setRemark(this.remark.trim());
        bill.setBillType(BillTypeEnum.ALIPAY.getValue());
        bill.setUserRemark(null);
        bill.setCategoryId(null);
        bill.setUserId(appUserId);
        bill.setBatchNo(batchNo);
        return bill;
    }

}
