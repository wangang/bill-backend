package org.rainwalk.bill.model.biz;

import cn.hutool.core.annotation.Alias;
import lombok.Data;
import org.rainwalk.bill.entity.Bill;
import org.rainwalk.bill.model.enums.BillChargeTypeEnum;
import org.rainwalk.bill.model.enums.BillStateEnum;
import org.rainwalk.bill.model.enums.BillTypeEnum;
import org.rainwalk.bill.model.enums.DescValueEnum;
import org.rainwalk.bill.util.IdGenerator;
import org.rainwalk.bill.util.MoneyUtil;
import org.rainwalk.bill.util.TimeUtil;

/**
 * 微信账单数据
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Data
public class WechatBill {

    @Alias("交易时间")
    private String createTime;

    @Alias("交易类型")
    private String transactionType;

    @Alias("交易对方")
    private String otherParty;

    @Alias("商品")
    private String goodsName;

    @Alias("收/支")
    private String chargeType;

    @Alias("金额(元)")
    private String amount;

    @Alias("支付方式")
    private String payType;

    @Alias("当前状态")
    private String stateDesc;

    @Alias("交易单号")
    private String transactionNo;

    @Alias("商户单号")
    private String merchantOrderNo;

    @Alias("备注")
    private String remark;

    public Bill toBill(String appUserId, String batchNo) {
        Bill bill = new Bill();
        bill.setId(IdGenerator.generateId());
        bill.setTransactionNo(this.transactionNo.trim());
        bill.setMerchantOrderNo(this.merchantOrderNo.trim());
        bill.setCreateTime(TimeUtil.toTimestamp(this.createTime.trim()));
        bill.setCreateTimePoint(TimeUtil.toTimePoint(this.createTime.trim()));
        bill.setTransactionType(this.transactionType.trim());
        bill.setOtherParty(this.otherParty.trim());
        bill.setGoodsName(this.goodsName.trim());
        bill.setAmount(MoneyUtil.yuan2fen(getAmountSmart()));
        bill.setChargeType(BillChargeTypeEnum.getByAmount(this.chargeType.trim()).getValue());
        bill.setStateDesc(this.stateDesc.trim());
        bill.setState(BillStateEnum.SUCCESS.getValue());
        bill.setServiceFee(0);
        bill.setRefundAmount(getRefundAmount(bill.getChargeType()));
        bill.setRemark(this.remark.trim());
        bill.setBillType(BillTypeEnum.WECHAT.getValue());
        bill.setUserRemark(null);
        bill.setCategoryId(null);
        bill.setUserId(appUserId);
        bill.setBatchNo(batchNo);
        return bill;
    }

    private String getAmountSmart() {
        if (this.amount.trim().startsWith("¥")) {
             return this.amount.trim().substring(1);
        } else if (this.amount.trim().startsWith("￥")) {
            return this.amount.trim().substring(1);
        }
        return this.amount.trim();
    }

    private int getRefundAmount(Integer chargeType) {
        DescValueEnum valueEnum = DescValueEnum.getByValue(chargeType, BillChargeTypeEnum.class);
        //收入账单不计退款
        if (BillChargeTypeEnum.INCOME.equals(valueEnum)) {
            return 0;
        }

        //情况一：已退款￥11.86
        //情况二：已退款(￥11.86)
        //情况三：退款中(￥459.00已退款，￥0.00退款中)
        //情况四：已全额退款

        //情况一和情况二处理
        if (this.stateDesc.contains("已退款") && ! this.stateDesc.contains("退款中")) {
            int begin = this.stateDesc.indexOf("￥");
            int end = this.stateDesc.indexOf(")");
            if (end == -1) {
                return MoneyUtil.yuan2fen(stateDesc.substring(begin + 1));
            }
            return MoneyUtil.yuan2fen(stateDesc.substring(begin + 1, end));
        }
        //情况三处理
        if (this.stateDesc.contains("退款中") && this.stateDesc.contains("已退款")) {
            int begin = this.stateDesc.indexOf("￥");
            int end = this.stateDesc.indexOf("已退款");
            return MoneyUtil.yuan2fen(stateDesc.substring(begin + 1, end));
        }
        //情况四处理
        if (this.stateDesc.contains("已全款退款") && this.stateDesc.contains("已全额退款")) {
            return MoneyUtil.yuan2fen(this.amount.startsWith("¥") ? this.amount.substring(1) : this.amount);
        }

        return 0;
    }
}
