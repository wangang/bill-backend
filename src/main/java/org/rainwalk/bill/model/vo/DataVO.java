package org.rainwalk.bill.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 数据请求响应结构
 *
 * @author 趁雨行
 */
@Data
@AllArgsConstructor
public class DataVO {

    Object value;

    Object label;

}
