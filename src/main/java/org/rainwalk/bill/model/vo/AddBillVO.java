package org.rainwalk.bill.model.vo;

import lombok.Data;

/**
 * 账单添加响应
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Data
public class AddBillVO {

    private String batchNo;

    private Integer total;

    /**
     * 非中性账单（中性账单即计费类型为空，不导入系统）
     */
    private Long nonNeutralBill;

    private Integer success;

    private Integer duplicate;

    public AddBillVO(Integer total) {
        this.total = total;
        this.success = 0;
        this.duplicate = 0;
        this.nonNeutralBill = 0L;
    }

    public void successIncrease(Integer num) {
        this.success += num;
    }

    public void duplicateIncrease(Integer num) {
        this.duplicate += num;
    }

}
