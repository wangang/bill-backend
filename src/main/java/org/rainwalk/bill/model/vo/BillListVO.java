package org.rainwalk.bill.model.vo;

import lombok.Data;

/**
 * 响应账单信息
 *
 * @author chenyuheng create class on 2021-02-20
 */
@Data
public class BillListVO {

    private String id;

    /**
     * 交易单号
     */
    private String transactionNo;

    /**
     * 商户单号
     */
    private String merchantOrderNo;

    /**
     * 交易时间
     */
    private Integer createTime;

    /**
     * 交易类型
     */
    private String transactionType;

    /**
     * 交易对方
     */
    private String otherParty;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 交易金额
     */
    private Integer amount;

    /**
     * 计费类型 -1：支出  0：未知  1：收入
     */
    private Integer chargeType;

    /**
     * 交易状态描述
     */
    private String stateDesc;

    /**
     * 交易状态 1：成功 2：其他
     */
    private Integer state;

    /**
     * 服务费
     */
    private Integer serviceFee;

    /**
     * 成功退款
     */
    private Integer refundAmount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 账单类型 0：用户写入 1：微信 2：支付宝
     */
    private Integer billType;

    /**
     * 用户备注
     */
    private String userRemark;

    /**
     * 用户消费类别
     */
    private String categoryName;

    /**
     * 用户消费类别id
     */
    private String categoryId;

    /**
     * 落库批次号
     */
    private String batchNo;

    /**
     * 图标id
     */
    private String iconId;

    /**
     * 图标名称
     */
    private String iconName;

    /**
     * icon类
     */
    private String fontClass;

    /**
     * icon码
     */
    private String unicode;
}
