package org.rainwalk.bill.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 分类占比响应结构
 *
 * @author chenyuheng create class on 2021-02-24
 */
@Data
public class CategoryProportionVO {

    private Integer createTime;

    private String categoryName;

    /**
     * 单位为分
     */
    private Integer amount;

}
