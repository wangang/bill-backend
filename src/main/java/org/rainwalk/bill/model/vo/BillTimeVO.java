package org.rainwalk.bill.model.vo;

import lombok.Data;

/**
 * 账单时间
 *
 * @author chenyuheng create class on 2021-02-24
 */
@Data
public class BillTimeVO {

    private Integer maxTime;

    private Integer minTime;
}
