package org.rainwalk.bill.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 用户分类响应结构
 *
 * @author 趁雨行
 */
@Data
public class UserCategoryVO {

    private String id;

    /**
     * 类别名称
     */
    private String categoryName;

    /**
     * 图标id
     */
    private String iconId;

    /**
     * 图标名称
     */
    private String iconName;

    /**
     * icon类
     */
    private String fontClass;

    /**
     * icon码
     */
    private String unicode;

}
