package org.rainwalk.bill.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 账单合计
 *
 * @author 趁雨行
 */
@Data
@AllArgsConstructor
public class BillStatisticsVO {

    /**
     * 支出
     */
    private Integer expenditure;

    /**
     * 收入
     */
    private Integer income;

    /**
     * 服务费
     */
    private Integer serviceFee;

    /**
     * 退款
     */
    private Integer refundAmount;


}
