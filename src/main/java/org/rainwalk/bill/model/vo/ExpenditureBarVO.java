package org.rainwalk.bill.model.vo;

import lombok.Data;

/**
 * 支出柱状图响应结构
 *
 * @author chenyuheng create class on 2021-02-25
 */
@Data
public class ExpenditureBarVO {

    private Integer billType;

    private Integer createTime;

    private Integer amount;

}
