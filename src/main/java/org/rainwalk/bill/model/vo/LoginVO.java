package org.rainwalk.bill.model.vo;

import lombok.Data;

/**
 * 登陆响应
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Data
public class LoginVO {

    private String header;

    private String token;

    private String name;
}
