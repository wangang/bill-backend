package org.rainwalk.bill.api.formatter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 枚举转换
 *
 * @author chenyuheng create class on 2021-02-20
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EnumFormatters {

    EnumFormatter[] value();
}
