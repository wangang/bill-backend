package org.rainwalk.bill.api.formatter.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 时间转换：时间戳 -> 时间格式（默认 yyyy-MM-dd HH:mm:ss)
 *
 * @author chenyuheng create class on 2021-02-20
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TimeFormatter {

    /**
     * 描述：{"字段名|格式", "字段名（|省略格式）", ...}
     */
    String[] fields();
}
