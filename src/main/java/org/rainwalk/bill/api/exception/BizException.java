package org.rainwalk.bill.api.exception;

import lombok.Getter;
import org.rainwalk.bill.api.model.ResponseCode;

/**
 * 业务错误
 *
 * @author 趁雨行
 */
@Getter
public class BizException extends RuntimeException {

    private final ResponseCode responseCode;

    private final String message;

    public BizException(ResponseCode responseCode) {
        super(responseCode.getMessage());
        this.responseCode = responseCode;
        this.message = responseCode.getMessage();
    }

    public BizException(ResponseCode responseCode, String message) {
        super(message);
        this.responseCode = responseCode;
        this.message = message;
    }
}
