package org.rainwalk.bill.api.advice;

import org.rainwalk.bill.api.annotation.Crypto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;

import java.nio.charset.StandardCharsets;

/**
 * 应用加密
 *
 * @author chenyuheng create class on 2021-01-15
 */
public abstract class AbstractAppCrypto {

    @Value("${app.crypto.charset:UTF-8}")
    protected String charset = StandardCharsets.UTF_8.displayName();

    protected static boolean isNeed(MethodParameter parameter) {
        Crypto annotation;
        if (parameter.hasMethodAnnotation(Crypto.class)) {
            annotation = parameter.getMethodAnnotation(Crypto.class);
        } else {
            annotation = parameter.getContainingClass().getAnnotation(Crypto.class);
        }

        if (annotation == null) {
            return false;
        }

        return annotation.value();
    }



}
