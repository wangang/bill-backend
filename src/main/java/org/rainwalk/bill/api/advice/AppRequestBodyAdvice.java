package org.rainwalk.bill.api.advice;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.IoUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

/**
 * 应用请求参数加密
 *
 * @author chenyuheng create class on 2021-01-15
 */
@Slf4j
@ControllerAdvice
@ConditionalOnProperty(prefix = "app.crypto", name = "enable", havingValue = "true")
public class AppRequestBodyAdvice extends AbstractAppCrypto implements RequestBodyAdvice {

    @Value("${app.crypto.private-key}")
    private String privateKey;

    private RSA rsa;

    @PostConstruct
    public void init() {
        rsa = new RSA(privateKey, null);
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
        return true;
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) throws IOException {
        if (!isNeed(methodParameter)) {
            return httpInputMessage;
        }
        InputStream body = httpInputMessage.getBody();
        String read = IoUtil.read(body, StandardCharsets.UTF_8);
        byte[] decrypt = rsa.decrypt(Base64.decode(read), KeyType.PrivateKey);
        return new CryptoHttpInputMessage(httpInputMessage, decrypt);
    }

    @Override
    public Object afterBodyRead(Object o, HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
        return o;
    }

    @Override
    public Object handleEmptyBody(Object o, HttpInputMessage httpInputMessage, MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass) {
        return o;
    }

    public static class CryptoHttpInputMessage implements HttpInputMessage {

        private final HttpInputMessage inputMessage;

        private final byte[] body;

        public CryptoHttpInputMessage(HttpInputMessage inputMessage, byte[] body) {
            this.inputMessage = inputMessage;
            this.body = body;
        }

        @Override
        public InputStream getBody() throws IOException {
            return new ByteArrayInputStream(body);
        }

        @Override
        public HttpHeaders getHeaders() {
            return inputMessage.getHeaders();
        }
    }

}
