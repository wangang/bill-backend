package org.rainwalk.bill.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * http响应结构
 *
 * @author 趁雨行
 */
@Getter
@ApiModel("响应结构")
public class Response {

    /**
     * 状态码
     */
    @ApiModelProperty(value = "状态码")
    private final int code;

    /**
     * 是否成功处理
     */
    @ApiModelProperty(value = "是否成功处理")
    private final boolean isSuccess;

    /**
     * 响应信息
     */
    @ApiModelProperty(value = "响应信息")
    private final String message;

    /**
     * 业务数据
     */
    @Setter
    @ApiModelProperty(value = "业务数据")
    private Object data;

    private static final Response SUCCESS = new Response(ResponseCode.SUCCESS);
    private static final Response ERROR = new Response(ResponseCode.SERVER_ERROR);

    private Response(ResponseCode responseCode) {
        this.code = responseCode.getCode();
        this.isSuccess = responseCode.isSuccess();
        this.message = responseCode.getMessage();
        this.data = null;
    }

    private Response(ResponseCode responseCode, Object data) {
        this.code = responseCode.getCode();
        this.isSuccess = responseCode.isSuccess();
        this.message = responseCode.getMessage();
        this.data = data;
    }

    private Response(ResponseCode responseCode, String message) {
        this.code = responseCode.getCode();
        this.isSuccess = responseCode.isSuccess();
        this.message = message;
        this.data = null;
    }

    private Response(ResponseCode responseCode, String message, Object data) {
        this.code = responseCode.getCode();
        this.isSuccess = responseCode.isSuccess();
        this.message = message;
        this.data = data;
    }

    public static Response success() {
        return SUCCESS;
    }

    public static Response success(String message) {
        return new Response(ResponseCode.SUCCESS, message);
    }

    public static Response success(Object data) {
        return new Response(ResponseCode.SUCCESS, data);
    }

    public static Response success(String message, Object data) {
        return new Response(ResponseCode.SUCCESS, message, data);
    }

    public static Response error() {
        return ERROR;
    }

    public static Response fail(ResponseCode responseCode) {
        return new Response(responseCode);
    }

    public static Response fail(ResponseCode responseCode, String message) {
        return new Response(responseCode, message);
    }

}
