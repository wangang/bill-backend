package org.rainwalk.bill.api.model;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.Getter;
import org.rainwalk.bill.api.exception.BizException;
import org.rainwalk.bill.entity.AppUser;

/**
 * session用户结构
 */
@Getter
public class SessionUser {

    public SessionUser(String id) {
        if (StrUtil.isBlank(id)) {
            throw new BizException(ResponseCode.UNAUTHORIZED);
        }
        this.id = id;
    }

    public SessionUser(AppUser appUser) {
        if (StrUtil.isBlank(appUser.getId())) {
            throw new BizException(ResponseCode.UNAUTHORIZED);
        }
        this.id = appUser.getId();
    }

    private String id;
}
