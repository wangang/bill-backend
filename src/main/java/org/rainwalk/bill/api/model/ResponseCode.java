package org.rainwalk.bill.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 响应状态码及其信息枚举
 *
 * @author 趁雨行
 */
@Getter
@AllArgsConstructor
public enum ResponseCode {

    SUCCESS("操作成功", 200, true),
    PARAMS_VERITY_FAIL("参数校验失败", 412, false),
    RESOURCE_NOT_FOUND("操作成功", 404, false),
    UNAUTHORIZED("请先登录", 401, false),
    INSUFFICIENT_PERMISSIONS("权限不足", 403, false),
    SERVER_ERROR("服务器错误，请稍后再试", 500, false),

    ;

    /**
     * 响应信息
     */
    private final String message;
    /**
     * 状态码
     */
    private final int code;
    /**
     * 是否成功处理
     */
    private final boolean isSuccess;

}
