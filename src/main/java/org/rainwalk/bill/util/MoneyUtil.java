package org.rainwalk.bill.util;

import java.math.BigDecimal;

public class MoneyUtil {

    public static Integer yuan2fen(String yuan) {
        BigDecimal bigDecimal = new BigDecimal(yuan);
        return bigDecimal.movePointRight(2).intValue();
    }

    public static String fen2yuan(Integer fen) {
        BigDecimal bigDecimal = new BigDecimal(fen);
        return bigDecimal.movePointLeft(2).stripTrailingZeros().toPlainString();
    }
}
