package org.rainwalk.bill.util;

import cn.hutool.core.util.RandomUtil;

/**
 * id生成器
 *
 * @author chenyuheng create class on 2021-01-21
 */
public class IdGenerator {

    public static String generateId() {
        return System.currentTimeMillis() + RandomUtil.randomNumbers(5);
    }

}
