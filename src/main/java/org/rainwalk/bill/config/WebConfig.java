package org.rainwalk.bill.config;

import lombok.RequiredArgsConstructor;
import org.rainwalk.bill.api.interceptor.AuthInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * mvc配置
 *
 * @author chenyuheng create class on 2021-02-18
 */
@Configuration
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class WebConfig implements WebMvcConfigurer {

    private final AuthInterceptor authInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/**")
                //过滤knife4j请求
                .excludePathPatterns("/doc.html")
                .excludePathPatterns("/v2/api-docs/**")
                .excludePathPatterns("/swagger-resources/**")
                .excludePathPatterns("/webjars/**");
    }

}
