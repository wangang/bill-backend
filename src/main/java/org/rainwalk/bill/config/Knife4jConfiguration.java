package org.rainwalk.bill.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.rainwalk.bill.api.model.SessionUser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpServletRequest;

@Configuration
@EnableSwagger2
@EnableKnife4j
public class Knife4jConfiguration {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                //忽略参数类型
                .ignoredParameterTypes(SessionUser.class, HttpServletRequest.class)
                .apiInfo(new ApiInfoBuilder()
                        .title("懒人账单api文档")
                        .description("懒人账单api文档")
                        .termsOfServiceUrl("https://gitee.com/cyh724")
                        .contact(new Contact("趁雨行", "https://gitee.com/cyh724", "jack724@126.com"))
                        .version("1.0")
                        .build())
                        //分组名称
                        .groupName("1.0版本")
                        .select()
                        //这里指定Controller扫描包路径
                        .apis(RequestHandlerSelectors.basePackage("org.rainwalk.bill.controller"))
                        .paths(PathSelectors.any())
                        .build();
        return docket;
    }

}
