package org.rainwalk.bill.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户消费类别
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("category")
public class Category extends Model<Category> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 类别名称
     */
    @TableField("category_name")
    private String categoryName;

    /**
     * 用户标识
     */
    @TableField("user_id")
    private String userId;

    /**
     * 图标id
     */
    @TableField("icon_id")
    private String iconId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
