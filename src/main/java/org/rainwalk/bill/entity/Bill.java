package org.rainwalk.bill.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 账单
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("bill")
public class Bill extends Model<Bill> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 交易单号
     */
    @TableField("transaction_no")
    private String transactionNo;

    /**
     * 商户单号
     */
    @TableField("merchant_order_no")
    private String merchantOrderNo;

    /**
     * 交易时间
     */
    @TableField("create_time")
    private Integer createTime;

    /**
     * 交易时间点
     */
    @TableField("create_time_point")
    private Integer createTimePoint;

    /**
     * 交易类型
     */
    @TableField("transaction_type")
    private String transactionType;

    /**
     * 交易对方
     */
    @TableField("other_party")
    private String otherParty;

    /**
     * 商品名称
     */
    @TableField("goods_name")
    private String goodsName;

    /**
     * 交易金额
     */
    @TableField("amount")
    private Integer amount;

    /**
     * 计费类型 -1：支出  0：未知  1：收入
     */
    @TableField("charge_type")
    private Integer chargeType;

    /**
     * 交易状态描述
     */
    @TableField("state_desc")
    private String stateDesc;

    /**
     * 交易状态 1：成功 2：其他
     */
    @TableField("state")
    private Integer state;

    /**
     * 服务费
     */
    @TableField("service_fee")
    private Integer serviceFee;

    /**
     * 成功退款
     */
    @TableField("refund_amount")
    private Integer refundAmount;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 账单类型 0：用户写入 1：微信 2：支付宝
     */
    @TableField("bill_type")
    private Integer billType;

    /**
     * 用户备注
     */
    @TableField(value = "user_remark")
    private String userRemark;

    /**
     * 用户消费类别
     */
    @TableField(value = "category_id")
    private String categoryId;

    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 落库批次号
     */
    @TableField("batch_no")
    private String batchNo;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
