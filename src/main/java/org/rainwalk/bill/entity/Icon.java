package org.rainwalk.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 类别图标-iconfont图标库
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Icon implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 图标名称
     */
    private String iconName;

    /**
     * icon类
     */
    private String fontClass;

    /**
     * icon码
     */
    private String unicode;

    /**
     * icon类别
     */
    private String iconCategoryId;


}
