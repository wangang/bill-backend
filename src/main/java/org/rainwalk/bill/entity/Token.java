package org.rainwalk.bill.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 登陆凭证
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("token")
public class Token extends Model<Token> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 用户id
     */
    @TableField("user_id")
    private String userId;

    /**
     * 获取凭证时间
     */
    @TableField("refresh_time")
    private Integer refreshTime;

    /**
     * 请求ip地址
     */
    @TableField("ip")
    private String ip;

    /**
     * 微信openid
     */
    @TableField("openid")
    private String openid;

    /**
     * 微信session维护字段
     */
    @TableField("session_key")
    private String sessionKey;

    /**
     * 登录方式 0-pc 1-小程序
     */
    @TableField("login_type")
    private Integer loginType;

    /**
     * 状态 0-有效 1-失效
     */
    @TableField("state")
    private Integer state;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
