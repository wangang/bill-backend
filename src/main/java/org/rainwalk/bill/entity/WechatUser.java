package org.rainwalk.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 微信小程序用户表
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WechatUser implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 微信小程序openid
     */
    @TableId(value = "openid", type = IdType.ID_WORKER_STR)
    private String openid;

    /**
     * 昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 头像图片地址
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 绑定的用户
     */
    @TableField("user_id")
    private String userId;

    /**
     * 绑定时间
     */
    @TableField("bind_time")
    private Integer bindTime;


}
