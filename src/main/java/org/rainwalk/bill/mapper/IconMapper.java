package org.rainwalk.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.rainwalk.bill.entity.Icon;

/**
 * <p>
 * 类别图标-iconfont图标库 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
public interface IconMapper extends BaseMapper<Icon> {

}
