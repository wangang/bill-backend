package org.rainwalk.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.rainwalk.bill.entity.IconCategory;

/**
 * <p>
 * 图标类别 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
public interface IconCategoryMapper extends BaseMapper<IconCategory> {

}
