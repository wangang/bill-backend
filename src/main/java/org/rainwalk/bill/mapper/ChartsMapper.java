package org.rainwalk.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.rainwalk.bill.entity.Bill;
import org.rainwalk.bill.model.vo.CategoryProportionVO;
import org.rainwalk.bill.model.vo.BillTimeVO;
import org.rainwalk.bill.model.vo.ExpenditureBarVO;

import java.util.List;

/**
 * 图表 Mapper 接口
 *
 * @author chenyuheng create class on 2021-02-24
 */
public interface ChartsMapper extends BaseMapper<Bill> {

    List<CategoryProportionVO> categoryProportion(@Param("start") int start, @Param("end") int end, @Param("userId") String appUserId, @Param("chargeType") Integer chargeType);

    BillTimeVO getBillTime(@Param("userId") String appUserId);

    List<ExpenditureBarVO> bar(@Param("start") int start, @Param("end") int end, @Param("userId") String appUserId, @Param("chargeType") Integer chargeType);
}
