package org.rainwalk.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.rainwalk.bill.entity.WechatUser;

/**
 * <p>
 * 微信小程序用户表 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-16
 */
public interface WechatUserMapper extends BaseMapper<WechatUser> {

}
