package org.rainwalk.bill.mapper;

import org.rainwalk.bill.entity.Token;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 登陆凭证 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface TokenMapper extends BaseMapper<Token> {

}
