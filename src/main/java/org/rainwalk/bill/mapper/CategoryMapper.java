package org.rainwalk.bill.mapper;

import org.apache.ibatis.annotations.Param;
import org.rainwalk.bill.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.rainwalk.bill.model.vo.UserCategoryVO;

import java.util.List;

/**
 * <p>
 * 用户消费类别 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface CategoryMapper extends BaseMapper<Category> {

    List<UserCategoryVO> queryByUser(@Param("userId") String userId);
}
