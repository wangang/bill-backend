package org.rainwalk.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.rainwalk.bill.entity.Bill;
import org.rainwalk.bill.model.po.BillQueryPO;
import org.rainwalk.bill.model.po.BillStatisticsPO;
import org.rainwalk.bill.model.vo.BillListVO;

import java.util.List;

/**
 * <p>
 * 账单 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface BillMapper extends BaseMapper<Bill> {

    /**
     * 分页查询
     *
     * @param po
     * @return
     */
    List<BillListVO> queryPage(@Param("po") BillQueryPO po);

    /**
     * 条件查询
     *
     * @param po
     * @return
     */
    List<BillListVO> queryList(@Param("po") BillQueryPO po);

    /**
     * 分页总数查询
     *
     * @param po
     * @return
     */
    int queryPageTotal(@Param("po") BillQueryPO po);

    /**
     * 合计查询
     *
     * @param po
     * @return
     */
    BillStatisticsPO queryStatistics(@Param("po") BillQueryPO po);

    /**
     * 账单不存在时新增账单（存在条件为 交易单号&&用户ID&&账单类型）
     *
     * @param bill
     * @return
     */
    int updateWhenNoExists(@Param("bill") Bill bill);


    /**
     * 根据账单id更新账单备注为null
     *
     * @param bill
     * @return
     */
    int updateUserRemark(@Param("bill") Bill bill);

    /**
     * 根据账单id更新账单类别为null
     *
     * @param bill
     * @return
     */
    int updateCategory(@Param("bill") Bill bill);

    /**
     * 根据账单id更新账单信息
     *
     * @param bill
     * @return
     */
    int updateBill(@Param("bill") Bill bill);
}
