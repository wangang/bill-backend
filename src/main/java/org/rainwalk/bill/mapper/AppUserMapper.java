package org.rainwalk.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.rainwalk.bill.entity.AppUser;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface AppUserMapper extends BaseMapper<AppUser> {

}
