package org.rainwalk.bill.service;

import org.rainwalk.bill.entity.AppUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface IAppUserService extends IService<AppUser> {

}
