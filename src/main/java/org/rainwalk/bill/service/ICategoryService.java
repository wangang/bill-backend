package org.rainwalk.bill.service;

import org.rainwalk.bill.entity.Category;
import com.baomidou.mybatisplus.extension.service.IService;
import org.rainwalk.bill.model.vo.UserCategoryVO;

import java.util.List;

/**
 * <p>
 * 用户消费类别 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface ICategoryService extends IService<Category> {

    /**
     * 查找用户的分类
     *
     * @param userId 用户id
     */
    List<UserCategoryVO> queryByUser(String userId);
}
