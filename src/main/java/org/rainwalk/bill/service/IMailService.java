package org.rainwalk.bill.service;

import org.rainwalk.bill.entity.Token;
import org.springframework.scheduling.annotation.Async;

/**
 * 邮件服务
 *
 * @author 趁雨行
 */
public interface IMailService {

    /**
     * 登录邮件通知
     *
     * @param token 登录信息
     */
    void loginNotify(Token token);

    /**
     * 异常邮件通知
     *
     * @param throwable 错误信息
     */
    void errorNotify(Throwable throwable);
}
