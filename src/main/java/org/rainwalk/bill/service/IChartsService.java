package org.rainwalk.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.rainwalk.bill.entity.Bill;
import org.rainwalk.bill.model.enums.BillChargeTypeEnum;
import org.rainwalk.bill.model.vo.BillTimeVO;
import org.rainwalk.bill.model.vo.CategoryProportionVO;
import org.rainwalk.bill.model.vo.ExpenditureBarVO;

import java.util.List;

/**
 * 图表服务
 *
 * @author chenyuheng create class on 2021-02-24
 */
public interface IChartsService extends IService<Bill> {

    List<CategoryProportionVO> categoryProportion(int start, int end, String appUserId, BillChargeTypeEnum chargeType);

    BillTimeVO getBillTime(String appUserId);

    List<ExpenditureBarVO> bar(Integer start, Integer end, String appUserId, BillChargeTypeEnum expenditure);
}
