package org.rainwalk.bill.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.rainwalk.bill.entity.Bill;
import com.baomidou.mybatisplus.extension.service.IService;
import org.rainwalk.bill.model.po.BillQueryPO;
import org.rainwalk.bill.model.vo.AddBillVO;
import org.rainwalk.bill.model.vo.BillListVO;
import org.rainwalk.bill.model.vo.BillStatisticsVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 账单 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface IBillService extends IService<Bill> {

    /**
     * 导入微信账单
     *  @param csv
     * @param ignoreDuplicate
     * @param appUserId
     * @return
     */
    AddBillVO importWechatBill(MultipartFile csv, Boolean ignoreDuplicate, String appUserId);

    /**
     * 导入支付宝账单
     *  @param csv
     * @param ignoreDuplicate
     * @param appUserId
     * @return
     */
    AddBillVO importAlipayBill(MultipartFile csv, Boolean ignoreDuplicate, String appUserId);

    /**
     * 增量更新账单信息
     *
     * @param bill
     */
    void update(Bill bill);

    /**
     * 分页查询
     *
     * @param toPO
     * @return
     */
    Page<BillListVO> queryPage(BillQueryPO toPO);

    /**
     * 条件查询
     *
     * @param toPO
     * @return
     */
    List<BillListVO> queryList(BillQueryPO toPO);

    /**
     * 条件查询
     *
     * @param toPO
     * @return
     */
    List<Map<String, Object>> queryListFormatter(BillQueryPO toPO);

    /**
     * 账单合计
     *
     * @param toPo
     * @return
     */
    BillStatisticsVO queryStatistics(BillQueryPO toPo);

    /**
     * 根据账单id与用户获取账单
     *
     * @param id
     * @param appUserId
     * @return
     */
    Bill getByIdAndUser(String id, String appUserId);

    /**
     * 根据账单批次与用户获取账单
     *
     * @param batchNo
     * @param appUserId
     * @return
     */
    List<Bill> getByBatchNoAndUser(String batchNo, String appUserId);

    /**
     * 根据账单id更新账单备注为null
     *
     * @param bill
     */
    void updateUserRemark(Bill bill);

    /**
     * 根据账单id更新账单类别为null
     *
     * @param bill
     */
    void updateCategory(Bill bill);

    /**
     * 更新订单信息
     *
     * @param bill
     */
    void updateBill(Bill bill);
}
