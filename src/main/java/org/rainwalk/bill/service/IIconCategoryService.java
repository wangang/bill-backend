package org.rainwalk.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.rainwalk.bill.entity.IconCategory;

/**
 * <p>
 * 图标类别 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
public interface IIconCategoryService extends IService<IconCategory> {

}
