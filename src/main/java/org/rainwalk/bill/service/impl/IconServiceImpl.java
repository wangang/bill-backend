package org.rainwalk.bill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.rainwalk.bill.entity.Icon;
import org.rainwalk.bill.mapper.IconMapper;
import org.rainwalk.bill.service.IIconService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 类别图标-iconfont图标库 服务实现类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
@Service
public class IconServiceImpl extends ServiceImpl<IconMapper, Icon> implements IIconService {

}
