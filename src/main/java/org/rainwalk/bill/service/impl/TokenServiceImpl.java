package org.rainwalk.bill.service.impl;

import org.apache.poi.ss.formula.functions.T;
import org.rainwalk.bill.entity.Token;
import org.rainwalk.bill.mapper.TokenMapper;
import org.rainwalk.bill.model.enums.TokenStateEnum;
import org.rainwalk.bill.service.ITokenService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.rainwalk.bill.util.TimeUtil;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登陆凭证 服务实现类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
@Service
public class TokenServiceImpl extends ServiceImpl<TokenMapper, Token> implements ITokenService {

    @Override
    public void refresh(String id) {
        Token token = new Token();
        token.setId(id);
        token.setRefreshTime(TimeUtil.getNow());
        this.updateById(token);
    }

    @Override
    public void banById(String id) {
        Token token = new Token();
        token.setId(id);
        token.setState(TokenStateEnum.INVALID.getValue());
        this.updateById(token);
    }
}
