package org.rainwalk.bill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.rainwalk.bill.entity.Bill;
import org.rainwalk.bill.mapper.ChartsMapper;
import org.rainwalk.bill.model.enums.BillChargeTypeEnum;
import org.rainwalk.bill.model.vo.BillTimeVO;
import org.rainwalk.bill.model.vo.CategoryProportionVO;
import org.rainwalk.bill.model.vo.ExpenditureBarVO;
import org.rainwalk.bill.service.IChartsService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

/**
 * 图表服务实现
 *
 * @author chenyuheng create class on 2021-02-24
 */
@Service
public class ChartsServiceImpl extends ServiceImpl<ChartsMapper, Bill> implements IChartsService {

    @Override
    public List<CategoryProportionVO> categoryProportion(int start, int end, String appUserId, BillChargeTypeEnum chargeType) {
        return this.baseMapper.categoryProportion(start, end, appUserId, chargeType.getValue());
    }

    @Override
    public BillTimeVO getBillTime(String appUserId) {
        BillTimeVO billTime = this.baseMapper.getBillTime(appUserId);
        billTime.setMinTime((int) LocalDateTime.ofEpochSecond(billTime.getMinTime(), 0, ZoneOffset.ofHours(8))
                .toLocalDate()
                .atStartOfDay()
                .toEpochSecond(ZoneOffset.ofHours(8)));
        billTime.setMaxTime((int) LocalDateTime.ofEpochSecond(billTime.getMaxTime(), 0, ZoneOffset.ofHours(8))
                .toLocalDate()
                .atStartOfDay()
                .plusDays(1)
                .toEpochSecond(ZoneOffset.ofHours(8)));
        return billTime;
    }

    @Override
    public List<ExpenditureBarVO> bar(Integer start, Integer end, String appUserId, BillChargeTypeEnum chargeType) {
        return this.baseMapper.bar(start, end, appUserId, chargeType.getValue());
    }
}
