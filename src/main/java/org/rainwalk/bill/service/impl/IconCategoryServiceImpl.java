package org.rainwalk.bill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.rainwalk.bill.entity.IconCategory;
import org.rainwalk.bill.mapper.IconCategoryMapper;
import org.rainwalk.bill.service.IIconCategoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 图标类别 服务实现类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
@Service
public class IconCategoryServiceImpl extends ServiceImpl<IconCategoryMapper, IconCategory> implements IIconCategoryService {

}
