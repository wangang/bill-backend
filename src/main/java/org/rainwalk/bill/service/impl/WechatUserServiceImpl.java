package org.rainwalk.bill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.rainwalk.bill.entity.WechatUser;
import org.rainwalk.bill.mapper.WechatUserMapper;
import org.rainwalk.bill.service.IWechatUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 微信小程序用户表 服务实现类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-16
 */
@Service
public class WechatUserServiceImpl extends ServiceImpl<WechatUserMapper, WechatUser> implements IWechatUserService {

}
