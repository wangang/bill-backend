package org.rainwalk.bill.service.impl;

import org.rainwalk.bill.entity.Category;
import org.rainwalk.bill.mapper.CategoryMapper;
import org.rainwalk.bill.model.vo.UserCategoryVO;
import org.rainwalk.bill.service.ICategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户消费类别 服务实现类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

    @Override
    public List<UserCategoryVO> queryByUser(String userId) {
        return baseMapper.queryByUser(userId);
    }
}
