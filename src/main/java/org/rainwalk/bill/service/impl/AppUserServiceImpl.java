package org.rainwalk.bill.service.impl;

import org.rainwalk.bill.entity.AppUser;
import org.rainwalk.bill.mapper.AppUserMapper;
import org.rainwalk.bill.service.IAppUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements IAppUserService {

}
