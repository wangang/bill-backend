package org.rainwalk.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.rainwalk.bill.entity.Icon;

/**
 * <p>
 * 类别图标-iconfont图标库 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-18
 */
public interface IIconService extends IService<Icon> {

}
