package org.rainwalk.bill.service;

import org.rainwalk.bill.entity.Token;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 登陆凭证 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-02-18
 */
public interface ITokenService extends IService<Token> {

    void refresh(String id);

    void banById(String id);
}
