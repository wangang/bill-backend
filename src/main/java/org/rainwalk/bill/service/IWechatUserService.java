package org.rainwalk.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.rainwalk.bill.entity.WechatUser;

/**
 * <p>
 * 微信小程序用户表 服务类
 * </p>
 *
 * @author 趁雨行
 * @since 2021-10-16
 */
public interface IWechatUserService extends IService<WechatUser> {

}
