-- 微信小程序相关
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `wechat_user` (
                               `openid` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信小程序openid',
                               `nickname` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '昵称',
                               `avatar` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像图片地址',
                               `user_id` varchar(18) NOT NULL COMMENT '绑定的用户',
                               `bind_time` int NOT NULL COMMENT '绑定时间',
                               PRIMARY KEY (`openid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='微信小程序用户表';

ALTER TABLE `token`
    ADD COLUMN `openid` varchar(64) NULL COMMENT '微信openid' AFTER `ip`,
    ADD COLUMN `session_key` varchar(50) NULL DEFAULT NULL COMMENT '微信session维护字段' AFTER `openid`,
    ADD COLUMN `login_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '登录方式 0-pc 1-小程序' AFTER `session_key`,
    ADD COLUMN `state` tinyint(1) NOT NULL DEFAULT 0 AUTO_INCREMENT COMMENT '状态 0-有效 1-失效' AFTER `login_type`;
ALTER TABLE `bill`.`token`
    MODIFY COLUMN `login_type` tinyint(1) NOT NULL COMMENT '登录方式 0-pc 1-小程序' AFTER `openid`;

ALTER TABLE `bill`.`category`
    ADD COLUMN `icon_id` varchar(18) NULL DEFAULT NULL COMMENT 'icon id' AFTER `user_id`;

CREATE TABLE `icon` (
                        `id` varchar(18) COLLATE utf8mb4_general_ci NOT NULL,
                        `icon_name` varchar(20) COLLATE utf8mb4_general_ci NOT NULL COMMENT '图标名称',
                        `font_class` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'icon类',
                        `unicode` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'icon码',
                        `icon_category_id` varchar(18) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'icon类别',
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='类别图标-iconfont图标库';

CREATE TABLE `icon_category` (
                                 `id` varchar(18) COLLATE utf8mb4_general_ci NOT NULL,
                                 `category_name` varchar(20) COLLATE utf8mb4_general_ci NOT NULL COMMENT '类别名称',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='图标类别';

SET FOREIGN_KEY_CHECKS = 1;